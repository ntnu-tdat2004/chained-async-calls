# chained-async-calls

## Prerequisites
Install `Node.js`, `npm` and `git`.

## Clone repository
```sh
git clone https://gitlab.com/ntnu-tdat2004/chained-async-calls
cd chained-async-calls
```

## Install dependencies
```sh
npm install
```

## Run
```sh
npm start
```
